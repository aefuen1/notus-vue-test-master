import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Calendar from '../views/Calendar.vue';
import Users from '../views/Users.vue';
import TurnTemplates from '../views/TurnTemplates.vue';
import Locations from '../views/Locations.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/calendario',
    name: 'Calendar',
    component: Calendar,
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
  },
  {
    path: '/locations',
    name: 'Locations',
    component: Locations,
  },
  {
    path: '/turntemplates',
    name: 'TurnTemplates',
    component: TurnTemplates,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
