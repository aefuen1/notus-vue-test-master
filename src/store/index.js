import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
function getMaxId(input) {
  if (toString.call(input) !== "[object Array]"){
    return false;
  }
  return Math.max.apply(null, input);
}

export default new Vuex.Store({
  state: {
    userList: null,
    locationsList: null,
    positionsList: null,
    contractsList: null,
    turnTemplatesList: null,
    turnsList: null,
    turnsDates: null,
  },
  mutations: {
    setUsers(state, newUserList) {
      state.userList = newUserList;
    },
    setLocations(state, newLocationsList) {
      state.locationsList = newLocationsList;
    },
    setPositions(state, newPositionsList) {
      state.positionsList = newPositionsList;
    },
    setContracts(state, newContractsList) {
      state.contractsList = newContractsList;
    },
    setTurnTemplates(state, newTurnTemplatesList) {
      state.turnTemplatesList = newTurnTemplatesList;
    },
    setTurns(state, newTurnsList) {
      state.turnsList = newTurnsList;
    },
    setTurnsDates(state, newTurnsDatesList) {
      state.turnsDates = newTurnsDatesList;
    },
  },
  getters: {
    getPositionById: (state) => (payload) => {
      if (payload.id !== 11) {
        return state.positionsList.filter(
          (item) => payload.positionId === item.id
        )[0].name;
      } else {
        return state.positionsList.filter(
          (item) => payload.position === item.id
        )[0].name;
      }
    },
    getLocations: (state) => (turnTemplate) => {
      const ids =
        turnTemplate.id === 11
          ? turnTemplate.locations
          : turnTemplate.locationId;
      const locations = [];
      if (ids.length) {
        state.locationsList.forEach((location) => {
          ids.forEach((id) => {
            if (location.id === id) {
              locations.push(location.name);
            }
          });
        });
        return locations.join(', ');
      } else {
        return 'No tiene sucursal asociada';
      }
    },
    getUsersList: (state) => () => {
      return state.userList;
    }
  },
  actions: {
    UPDATE_USERS({ commit }) {
      return new Promise((resolve, reject) => {
        fetch('http://localhost:3004/users')
          .then((response) => response.json())
          .then((data) => {
            commit('setUsers', data);
            resolve(data);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    async fetchUsers({ commit }) {
      const res = await fetch('http://localhost:3004/users');
      const data = await res.json();
      data.sort((a, b) => {
        const id_a = parseInt(a.employeeId.replace(/[-.Q]/g, ''), 10);
        const id_b = parseInt(b.employeeId.replace(/[-.Q]/g, ''), 10);
        return id_a - id_b;
      });
      commit('setUsers', data);
    },
    async fetchLocations({ commit }) {
      const res = await fetch('http://localhost:3004/locations');
      const data = await res.json();
      data.sort((a, b) => a.name.localeCompare(b.name));
      commit('setLocations', data);
    },
    async fetchPositions({ commit }) {
      const res = await fetch('http://localhost:3004/positions');
      const data = await res.json();
      commit('setPositions', data);
    },
    async fetchContract({ commit }) {
      const res = await fetch('http://localhost:3004/contracts');
      const data = await res.json();
      commit('setContracts', data);
    },
    async fetchTurnTemplates({ commit }) {
      const res = await fetch('http://localhost:3004/turnTemplates');
      const data = await res.json();
      commit('setTurnTemplates', data);
    },
    async fetchTurns({ commit }) {
      const res = await fetch('http://localhost:3004/turns');
      const data = await res.json();
      data.sort((a, b) => new Date(a.date) - new Date(b.date));
      const dates = [];
      data.forEach((item) => {
        if (!dates.includes(item.date)) {
          dates.push(item.date);
        }
      });
      commit('setTurns', data);
      commit('setTurnsDates', dates);
    },
    async deleteUser({ commit, getters }, id){
      const list = getters.getUsersList().filter((item) => item.employeeId == id);
      const index = list.length ? list[0].id : null;
      if (index) {
          const res = await fetch(
              `http://localhost:3004/users/${index}`,
              {method: 'DELETE'}
          );
      }
      commit('setUsers', getters.getUsersList().filter((item) => item.employeeId !== id));
    },
    async addUser({ commit, getters }, user){
      const maxId = getMaxId(getters.getUsersList().map(item => item.id));
      const res = await fetch(
        `http://localhost:3004/users`,
        {
          method: 'POST',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            ...user,
            id: maxId + 1,
          }),
        }
      );
      commit('setUsers', [...getters.getUsersList(), {...user, id: maxId + 1}]);
    },
    async updateUser({ commit, getters }, user){
      const list = getters.getUsersList().filter((item) => item.employeeId == user.employeeId);
      const index = list.length ? list[0].id : null;
      const res = await fetch(
        `http://localhost:3004/users/${index}`,
        {
          method: 'PUT',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            ...user,
            id: index,
          }),
        }
      );
    },
  },
  modules: {},
});
